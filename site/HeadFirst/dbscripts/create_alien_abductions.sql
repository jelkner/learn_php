DROP TABLE IF EXISTS Abductions;

CREATE TABLE Abductions(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    email TEXT NOT NULL,
    occured TEXT NOT NULL,
    howlong TEXT NOT NULL,
    howmany TEXT NOT NULL,
    description TEXT NOT NULL,
    whattheydid TEXT NOT NULL,
    tabbyspotted INTEGER NOT NULL,
    other TEXT NOT NULL
);
