<?php
$dir = 'sqlite:database/acclib.db';
$dbh  = new PDO($dir) or die("Cannot open the database");
$query =  "SELECT * FROM Books";
foreach ($dbh->query($query) as $row)
{
    echo $row[0] . ' ' . substr($row[1], 0, 75) . "\n";
}
$dbh = null; //This is how you close a PDO connection
