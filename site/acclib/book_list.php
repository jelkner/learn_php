<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Book List</title>
<style>
@import url(style.css);
header > nav {
  text-align: center;
}
</style>
</head>
<body>
<header>
<h1>Book List</h1>
<nav>
<a href="index.php">Home</a>
</nav>
</header>

<main>
<?php
$dir = 'sqlite:database/acclib.db';
$dbh  = new PDO($dir) or die("Cannot open the database");
$query =  "SELECT * FROM Books";
foreach ($dbh->query($query) as $row)
{
    echo $row[1];
}
$dbh = null; //This is how you close a PDO connection
?>
</main>

<footer id="vLinks">
</footer>

<script>
var loc = window.location.href;

var linkStr = '<a href="http://validator.w3.org/check?uri=';
linkStr += loc;
linkStr += '">\n<strong> HTML </strong> Valid! </a> |';
linkStr +=' <a href="http://jigsaw.w3.org/css-validator/validator?uri='
linkStr += loc;
linkStr += '?profile=css3">\n<strong> CSS </strong> Valid! </a>'
console.log(linkStr);

var footer = document.getElementById("vLinks");
console.log(footer);
footer.innerHTML = linkStr;
</script>
</body>
</html>
