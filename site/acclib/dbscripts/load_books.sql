INSERT INTO Books VALUES(
    1,
    'Good Faith Collaboration: The Culture of Wikipedia',
    'AE',
    100,
    'R43',
    '2010',
    '9780262014472',
    'shelf'
);
INSERT INTO Books VALUES(
    2,
    'Algorithms to Live By: The Computer Science of Human Decisions',
    'BF',
    39,
    'C4885',
    '2016',
    '9781627790369',
    'shelf'
);
INSERT INTO Books VALUES(
    3,
    'Ours to Hack and to Own: The Rise of Platform Cooperativism, A New Vision for the Future of Work and a Fairer Internet',
    'HM',
    851,
    'O973',
    '2016',
    '9781944869335',
    'shelf'
);
INSERT INTO Books VALUES(
    4,
    'The Mathematics of Elections and Voting',
    'JF',
    1001,
    'W35',
    '2014',
    '9783319098098',
    'shelf'
);
INSERT INTO Books VALUES(
    5,
    'Beyond Transparency: Open Data and the Future of Civic Innovation',
    'JK',
    468,
    'T7 B49',
    '2013',
    '9780615889085',
    'shelf'
);
INSERT INTO Books VALUES(
    6,
    'Trusting Teachers with School Success: What Happens when Teachers Call the Shots',
    'LB',
    2806.45,
    'F36',
    '2013',
    '9781610485104',
    'shelf'
);
INSERT INTO Books VALUES(
    7,
    'Murphy''s Law and Other Reasons Why Things Go Wrong',
    'PN',
    6231,
    'M82 B57',
    '',
    '0843104287',
    'shelf'
);
INSERT INTO Books VALUES(
    8,
    'The Annotated Alice: Alice''s Adventures in Wonderland & Through the Looking Glass',
    'PR',
    4611,
    'A7',
    '1960',
    '9780452010413',
    'shelf'
);
INSERT INTO Books VALUES(
    9,
    'Gödel, Escher, Bach: An Eternal Golden Braid',
    'QA',
    9.8,
    'H63',
    '1980',
    '0394745027',
    'shelf'
);
INSERT INTO Books VALUES(
    10,
    'Doing Math with Python: Use Programming to Explore Algebra, Statistics, Calculus, and More!',
    'QA',
    20,
    'C65 S24',
    '2015',
    '9781593276409',
    'shelf'
);
INSERT INTO Books VALUES(
    11,
    'Rethinking Mathematics: Teaching Social Justice by the Numbers',
    'QA',
    22.3,
    'R478',
    '2013',
    '9780942961553',
    'shelf'
);
INSERT INTO Books VALUES(
    12,
    'Unlocking the Clubhouse: Women in Computing',
    'QA',
    76.25,
    'M35',
    '2001',
    '9780262632690',
    'shelf'
);
INSERT INTO Books VALUES(
    13,
    'Stuck in the Shallow End: Education, Race, and Computing',
    'QA',
    76.27,
    'M347',
    '2008eb',
    '9780262514040',
    'shelf'
);
INSERT INTO Books VALUES(
    14,
    'Turtles, Termites, and Traffic Jams: Explorations in Massively Parallel Microworlds',
    'QA',
    76.58,
    'R47',
    '1994',
    '9780262680936',
    'shelf'
);
INSERT INTO Books VALUES(
    15,
    'The C Trilogy: A Complete Library for C Programmers',
    'QA',
    76.73,
    'C15 B56',
    '1987',
    '0830628908',
    'shelf'
);
INSERT INTO Books VALUES(
    16,
    'The C Programming Language',
    'QA',
    76.73,
    'C15 K47',
    '1988',
    '9780131103702',
    'shelf'
);
INSERT INTO Books VALUES(
    17,
    'The C Answer Book: Solutions to the Exercises in The C Programming Language, Second Edition, by Brian W. Kernighan and Dennis M. Ritchie',
    'QA',
    76.73,
    'C15 K47',
    '1988 suppl',
    '9780131096530',
    'shelf'
);
INSERT INTO Books VALUES(
    18,
    'C Programming: A Complete Guide to Mastering the C Language',
    'QA',
    76.73,
    'C15 H364',
    '1989',
    '9780201194449',
    'shelf'
);

INSERT INTO Books VALUES(
    19,
    'Data Structures Using C',
    'QA',
    76.73,
    'C15 T46',
    '1990',
    '9780131997462',
    'shelf'
);
INSERT INTO Books VALUES(
    20,
    'C: A Reference Manual',
    'QA',
    76.73,
    'C15 H38',
    '1991',
    '9780131109339',
    'shelf'
);
INSERT INTO Books VALUES(
    21,
    'The Beginner''s Guide to C',
    'QA',
    76.73,
    'C15 H668',
    '1994',
    '9781874416159',
    'shelf'
);
INSERT INTO Books VALUES(
    22,
    'C Primer Plus',
    'QA',
    76.73,
    'C15 P733',
    '1988',
    '0672225824',
    'shelf'
);
INSERT INTO Books VALUES(
    23,
    'C: The Complete Reference',
    'QA',
    76.73,
    'C15 S353',
    '1995',
    '9780078821011',
    'shelf'
);
INSERT INTO Books VALUES(
    24,
    'Eloquent JavaScript, 3rd Edition: A Modern Introduction to Programming',
    'QA',
    76.73,
    'J39 H38',
    '2019',
    '9781593279509',
    'shelf'
);
INSERT INTO Books VALUES(
    25,
    'Head First JavaScript Programming',
    'QA',
    76.73,
    'J39 F735',
    '2014',
    '9781449340131',
    'shelf'
);
INSERT INTO Books VALUES(
    26,
    'Javascript: The Good Parts',
    'QA',
    76.73,
    'J39 C763',
    '2008',
    '9780596517748',
    'shelf'
);
INSERT INTO Books VALUES(
    27,
    'Python Testing Beginner''s Guide: An Easy and Convenient Approach to Testing Your Python Projects',
    'QA',
    76.73,
    'P98 A73',
    '2010eb',
    '9781847198846',
    'shelf'
);
INSERT INTO Books VALUES(
    28,
    'Data Structures and Algorithms in Python',
    'QA',
    76.73,
    'P98 G66',
    '2013',
    '9788126562176',
    'shelf'
);
INSERT INTO Books VALUES(
    29,
    'Python Algorithms: Mastering Basic Algorithms in the Python Language',
    'QA',
    76.73,
    'P98 H485',
    '2010',
    '9781430232377',
    'shelf'
);
INSERT INTO Books VALUES(
    30,
    'Python for Data Analysis',
    'QA',
    76.73,
    'P98 M42',
    '2013',
    '9781449319793',
    'shelf'
);
INSERT INTO Books VALUES(
    31,
    'Introduction to Computer Science Using Python: A Computational Problem-Solving Focus',
    'QA',
    76.73,
    'P98 D547',
    '2013',
    '9780470555156',
    'shelf'
);
INSERT INTO Books VALUES(
    32,
    'Think Python',
    'QA',
    76.73,
    'P98 D694',
    '2012',
    '9781449330729',
    'shelf'
);
INSERT INTO Books VALUES(
    33,
    'Problem Solving with Algorithms and Data Structures Using Python',
    'QA',
    76.73,
    'P98 M54',
    '2011',
    '9781590282571',
    'shelf'
);
INSERT INTO Books VALUES(
    34,
    'Python Testing Beginner''s Guide: An Easy and Convenient Approach to Testing Your Python Projects',
    'QA',
    76.73,
    'P98 A73',
    '2010',
    '9781847198846',
    'shelf'
);
INSERT INTO Books VALUES(
    35,
    'Mathematics for the Digital Age and Programming in Python',
    'QA',
    76.73,
    'P98 L58',
    '2010',
    '9780982477540',
    'shelf'
);
INSERT INTO Books VALUES(
    36,
    'Python Testing Cookbook over 70 Simple but Incredibly Effective Recipes for Taking Control of Automated Testing Using Powerful Python Testing Tools',
    'QA',
    76.73,
    'P98 T87',
    '2011',
    '9781849514668',
    'shelf'
);
INSERT INTO Books VALUES(
    37,
    'The SQL Guide to SQLite',
    'QA',
    76.73,
    'S67 L3617',
    '2009',
    '9780557076765',
    'shelf'
);
INSERT INTO Books VALUES(
    38,
    'The Definitive Guide to Sqlite, Second Edition',
    'QA',
    76.73,
    'S67 A45',
    '2010eb',
    '9781430232254',
    'shelf'
);
INSERT INTO Books VALUES(
    39,
    'Test-Driven Development: A Practical Guide',
    'QA',
    76.76,
    'D47 A783',
    '2003',
    '9780131016491',
    'shelf'
);
INSERT INTO Books VALUES(
    40,
    'Clean Code: A Handbook of Agile Software Craftsmanship',
    'QA',
    76.76,
    'D47 C583',
    '2009',
    '9780132350884',
    'shelf'
);
INSERT INTO Books VALUES(
    41,
    'Learning the Vi Editor',
    'QA',
    76.76,
    'O63 L355',
    '1990',
    '9780937175675',
    'shelf'
);
INSERT INTO Books VALUES(
    42,
    'The UNIX-Haters Handbook',
    'QA',
    76.76,
    'O63 U54518',
    '1994',
    '9781568842035',
    'shelf'
);
INSERT INTO Books VALUES(
    43,
    'Test-Driven Development: By Example',
    'QA',
    76.76,
    'T48 B43',
    '2003',
    '9780321146533',
    'shelf'
);
INSERT INTO Books VALUES(
    44,
    'Assembly Language for x86 Processors: Sixth Edition',
    'QA',
    76.8,
    'I77',
    '2011',
    '9780136022121',
    'shelf'
);
INSERT INTO Books VALUES(
    45,
    'Data Structure and Algorithmic Thinking with Python',
    'QA',
    76.9,
    'A43 K368',
    '2016',
    '9788192107592',
    'shelf'
);
INSERT INTO Books VALUES(
    46,
    'Data Structures and Algorithms with Python',
    'QA',
    76.9,
    'D35',
    '',
    '9783319130712',
    'shelf'
);
INSERT INTO Books VALUES(
    47,
    'Data Structures and Algorithms',
    'QA',
    76.9,
    'D35 A38',
    '1982',
    '0201000237',
    'shelf'
);
INSERT INTO Books VALUES(
    48,
    'Discrete Mathematics',
    'QA',
    76.9,
    'M35 R67',
    '1988',
    '0132154277',
    'shelf'
);
INSERT INTO Books VALUES(
    49,
    'Think Stats',
    'QA',
    276.4,
    'D69',
    '2014',
    '9781491907337',
    'shelf'
);
INSERT INTO Books VALUES(
    50,
    'Think Bayes',
    'QA',
    279.5,
    'D69',
    '2013',
    '9781449370787',
    'shelf'
);
