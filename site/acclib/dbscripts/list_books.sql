.mode column
.headers on

SELECT
    title, class, subclass, cutter, suppl
FROM
    Books
ORDER BY
    class ASC,
    subclass ASC,
    cutter ASC,
    suppl ASC;
