<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Learning PHP with TDD</title>
<style>
@import url(site/main.css);
</style>
</head>
<body>
<h1>Learning PHP with TDD</h1>

<main>
<h2>Projects</h2>
<ul>
<li><a href="site/HeadFirst">Head First PHP &amp; MySQL</a></li>
<li><a href="site/acclib">ACC CS / ICT Library</a></li>
</ul>
</main>

<footer id="vLinks">
</footer>

<script>
var loc = window.location.href;

var linkStr = '<a href="http://validator.w3.org/check?uri=';
linkStr += loc;
linkStr += '">\n<strong> HTML </strong> Valid! </a> |';
linkStr +=' <a href="http://jigsaw.w3.org/css-validator/validator?uri='
linkStr += loc;
linkStr += '?profile=css3">\n<strong> CSS </strong> Valid! </a>'
console.log(linkStr);

var footer = document.getElementById("vLinks");
console.log(footer);
footer.innerHTML = linkStr;
</script>
</body>
</html>
